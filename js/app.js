
Vue.component('song-item', {
   template: 
  '\
      <a class="list-group-item list-group-item-action active">\
        <div class="d-flex w-100 justify-content-between"><h5 class="mb-1" >{{ title }}</h5> <br> \
        <small><button type="button" class="btn btn-light" v-on:click="$emit(\'remove\')">-</button></small></div> <p> by {{ artist }}</p>\
      </a>\
    ',

  props: ['title', 'artist']
});

new Vue({
  el: '#song-list-example',
  data: {
    newSongText: '',
    newArtistText:'',
    songs: [
     {
         id: 1,
         title: 'Mandy',
         artist: 'Barry Manilow'
       },
       {
         id: 2,
         title: 'Endless Love',
         artist: 'Lionel Ritchie'
       },
       {
         id: 3,
         title: "Can't Cry Hard Enough",
         artist: 'William Brothers'
       }
    ],
    nextSongId: 4
  },
  methods: {
    addList: function () {
      this.songs.push({
        id: this.nextSongId++,
        title: this.newSongText,
        artist: this.newArtistText
      })
      this.newSongText = '',
      this.newArtistText=''
    }
  }
});
